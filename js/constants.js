const novels = [
  {
    examboard: ["AQA", "CCEA", "EDX"],
    title: "Frankenstein",
    author: "Mary Shelley",
    published: "1818",
    pages: 280,
    displayimage: "frank750.jpg",
    thumbnail: "frank100.jpg"
  },
  {
    examboard: ["OCR","CCEA", "EDX"],
    title: "Dr Jekyll and Mr Hyde",
    author: "Robert Louis Stevenson",
    published: "1886",
    pages: 141,
    displayimage: "jekyll750.jpg",
    thumbnail: "jekyll100.jpg",
  },
  {
    examboard: ["AQA", "OCR", "EDX"],
    title: "The Sign of the Four",
    author: "Arthur Conan Doyle",
    published: "1890",
    pages: 200,
    displayimage: "holmes750.jpg",
    thumbnail: "holmes100.jpg"
  },
  {
    examboard: ["AQA", "CCEA"],
    title: "Great Expectations",
    author: "Charles Dickens",
    published: "1861",
    pages: 544,
    displayimage: "expectations750.jpg",
    thumbnail: "expectations100.jpg"
  },
  {
    examboard: ["AQA", "CCEA"],
    title: "The War of the Worlds",
    author: "H G Wells",
    published: "1897",
    pages: 287,
    displayimage: "warofworlds750.jpg",
    thumbnail: "warofworlds100.jpg"
  },
  {
    examboard: ["AQA", "CCEA", "OCR"],
    title: "Jayne Eyre",
    author: "Charlotte Bronte",
    published: "1847",
    pages: 300,
    displayimage: "jeyre750.jpg",
    thumbnail: "jeyre100.jpg"
  },
  {
    examboard: ["AQA", "EDX"],
    title: "A Christmas Carol",
    author: "Charles Dickens",
    published: "1843",
    pages: 180,
    displayimage: "xmascarol750.jpg",
    thumbnail: "xmascarol100.jpg"
  },
];

const drama = [
  {
    examboard: ["AQA", "EDX"],
    title: "Macbeth",
    author: "William Shakespeare",
    published: "1606",
    displayimage: "macbeth750.jpg",
    thumbnail: "macbeth100.jpg"
  },
  {
    examboard: ["AQA", "EDX"],
    title: "Romeo and Juliet",
    author: "William Shakespeare",
    published: "1597",
    displayimage: "romeo750.jpg",
    thumbnail: "romeo100.jpg"
  },
  {
    examboard: ["AQA", "EDX", "CCEA"],
    title: "The Tempest",
    author: "William Shakespeare",
    published: "1611",
    displayimage: "tempest750.jpg",
    thumbnail: "tempest100.jpg"
  },
  {
    examboard: ["AQA", "EDX", "OCR"],
    title: "Much Ado About Nothing",
    author: "William Shakespeare",
    published: "1612",
    displayimage: "muchado750.jpg",
    thumbnail: "muchado100.jpg"
  },
  {
    examboard: ["AQA", "EDX", "CCEA", "OCR"],
    title: "The Merchant of Venice",
    author: "William Shakespeare",
    published: "1605",
    displayimage: "merchant750.jpg",
    thumbnail: "merchant100.jpg"
  },
  {
    examboard: ["AQA", "EDX", "CCEA"],
    title: "Julius Ceaser",
    author: "William Shakespeare",
    published: "1599",
    displayimage: "ceaser750.jpg",
    thumbnail: "ceaser100.jpg"
  },
];

const modernFiction = [
  {
    examboard: ["AQA", "CCEA", "EDX"],
    title: "Animal Farm",
    author: "George Orwell",
    published: "1945",
    pages: 112,
    displayimage: "animalfarm750.jpg",
    thumbnail: "animalfarm100.jpg"
  },
  {
    examboard: ["AQA", "CCEA", "OCR"],
    title: "Anita and Me",
    author: "Meera Syal",
    published: "2002",
    pages: 112,
    displayimage: "anitaandme750.jpg",
    thumbnail:"anitaandme100.jpg"
  },
  {
    examboard: ["AQA", "CCEA", "EDX"],
    title: "The Lord of the Flies",
    author: "William Golding",
    published: "1954",
    pages: 224,
    displayimage: "flies750.jpg",
    thumbnail:"flies100.jpg"
  },
  {
    examboard: ["AQA", "CCEA", "EDX", "OCR"],
    title: "To Kill A Mockingbird",
    author: "Harper Lee",
    published: "1960",
    pages: 224,
    displayimage: "mockingbird750.jpg",
    thumbnail: "mockingbird100.jpg"
  },
];

const novelPreamble = "The 19th Century produced a wealth of literature that delved into the relationship between the supernatural and the physical sciences. Which of these novels have you chosen to study ?"
const dramaPreamble = "If all the world is a stage which part of Shakespeare's world have you chosen to study ?"
const modernPreamble = "Ok, so it may not seem modern to you but which of these modern classics forms part of your study?"