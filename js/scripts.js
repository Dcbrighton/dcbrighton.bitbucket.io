console.log('JS load started');

document.querySelector('.hamburger').addEventListener('click', function(){

    document.querySelector('nav').classList.toggle('open');

} );

// These arrays 
var novelsConstantPosForSliderPos =[];
var dramaConstantPosForSliderPos =[];
var modernConstantPosForSliderPos =[];


 $('.board-button').click(function() {

  var chosenBoard = $(this).attr('data-name');

  novelsConstantPosForSliderPos =[];
  dramaConstantPosForSliderPos =[];
  modernConstantPosForSliderPos =[];


  clearDivs();

  loadSliderDivsWithData(novels,chosenBoard,"novel-slider","slider-nav",novelPreamble,1);
  loadSliderDivsWithData(drama,chosenBoard,"drama-slider","slider-nav-drama",dramaPreamble,2);
  loadSliderDivsWithData(modernFiction,chosenBoard,"modern-slider","slider-nav-modern",modernPreamble,3);

  initialiseAllSliders();

    // This needs to be within this function - how do i refcator it out ?

    $('.image-button').click(function() {
      var currentSlide = $('.novel-slider').slick('slickCurrentSlide');
      var currentSlideDrama = $('.drama-slider').slick('slickCurrentSlide');
      var currentSlideModern = $('.modern-slider').slick('slickCurrentSlide');
  
      var emailText = `Hi, I would like to find out more about Bright Tutoring. I am studying ${novels[novelsConstantPosForSliderPos[currentSlide]].title} and  ${drama[dramaConstantPosForSliderPos[currentSlideDrama]].title} and ${modernFiction[modernConstantPosForSliderPos[currentSlideModern]].title}`;
      $('#messageBox').attr('placeholder',emailText);
    });

}); 


function clearDivs (){
   
  $('.main-novel').html('');
  $('.main-drama').html('');
  $('.main-modern').html('');
  $('.owl-novel').html('');
  $('.owl-drama').html('');
  $('.owl-modern').html('');

}


function loadSliderDivsWithData(studyTopicArray,chosenBoard,sliderName,navSliderName,preAmble,iteration){
  var imageDiv ='';
  var bigSliderImages       = `<div class="slider ${sliderName} bigimage ">   `;
  var thumbnailSliderImages = `<div class="slider ${navSliderName} thumbnails  ">   `;

  for(var i = 0; i < studyTopicArray.length; i++){
      for (var j = 0; j < studyTopicArray[i].examboard.length; j++) {
          if (chosenBoard == studyTopicArray[i].examboard[j]) {
              imageDiv = `<div>
                            <p class="preamble">${preAmble}</p>
                            <h2>${studyTopicArray[i].title}</h2> 
                            <img src="./images/${studyTopicArray[i].displayimage}" class="img-fluid" alt="Responsive image" alt=""></img> 
                            <button class="btn btn-secondary image-button col-md-4" >Confirm your Choice</button>
                          </div>`;
              bigSliderImages = bigSliderImages + imageDiv;
              imageDiv = `<div> 
                            <img src="./images/${studyTopicArray[i].thumbnail}" height="100" width="150" alt="">
                          </div>`;
              thumbnailSliderImages = thumbnailSliderImages + imageDiv;
              if (iteration==1){
                  novelsConstantPosForSliderPos.push(i);}
              if (iteration==2){
                    dramaConstantPosForSliderPos.push(i);}
              if (iteration==3){
                  modernConstantPosForSliderPos.push(i);}

             
          }
      }
  }

  console.log(novelsConstantPosForSliderPos);
  console.log(dramaConstantPosForSliderPos);
  console.log(modernConstantPosForSliderPos);

  bigSliderImages = bigSliderImages + `</div>`;
  thumbnailSliderImages = thumbnailSliderImages + `</div>`;
 
  // console.log(bigSliderImages+thumbnailSliderImages);

  switch(iteration){
    case 1:  
        console.log('in 1');
        $(`.main-novel`).append(bigSliderImages+thumbnailSliderImages);
        $(`.owl-novel`).append(`</a><button id="myBtn">Learn More</button>`);
        break;
    case 2:  
    console.log('in 2');
        $(`.main-drama`).append(bigSliderImages+thumbnailSliderImages);
        $(`.owl-drama`).append(`</a><button id="myBtn">Learn More</button>`);
        break;
    case 3:  
    console.log('in 3');
        $(`.main-modern`).append(bigSliderImages+thumbnailSliderImages);
        $(`.owl-modern`).append(`</a><button id="myBtn">Learn More</button>`);
        break;
  }
  
};


function initialiseSlider(mainSliderName, navSliderName){

  $('.'+ mainSliderName).slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.'+ navSliderName,
    focusOnSelect: true,
    prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></div>',
    nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></div>'
  });

  $('.'+ navSliderName,).slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor:  '.' + mainSliderName,
    dots: false,
    focusOnSelect: true,
    centerMode: true,
    arrows: true,
    infinite: true,
    prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></div>',
    nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></div>'
  });

}
   
function initialiseAllSliders(){

  initialiseSlider("novel-slider","slider-nav");
  initialiseSlider("drama-slider","slider-nav-drama");
  initialiseSlider("modern-slider","slider-nav-modern");  

  // Get the modal
  var modal = document.getElementById("myModal");
  
  // Get the button that opens the modal
  var btn = document.getElementById("myBtn");
  
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];
  
  // When the user clicks the button, open the modal 
  btn.onclick = function() {
   
    console.log("2clickin");

    $('#modaltext').append($(this).value);    
     $('#modaltext').append("hello"); 
    modal.style.display = "block";
  }
  
  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }
  
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }

}

console.log('JS load complete');